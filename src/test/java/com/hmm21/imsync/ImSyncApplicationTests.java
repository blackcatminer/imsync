package com.hmm21.imsync;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

import org.junit.Rule;
import org.junit.jupiter.api.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.hmm21.action.UserGroupAction;
import com.hmm21.model.CwdUser;
import com.hmm21.model.ImUser;
import com.hmm21.restApi.JiraRestApi;
import com.hmm21.types.ActionType;
import com.hmm21.types.HmmConsts;
import com.hmm21.types.RoleType;

//@RunWith(SpringRunner.class)
@SpringBootTest
class ImSyncApplicationTests {

	@Autowired
	private IMUserDao imuserDao;
	
//	@Rule
//	public ExpectedException exceptionRule = ExpectedException.none();
	 
	
	@Test
	void getImUsers_updateGroup() {		
		
		System.out.println(" >>>>>>>>>>>>>>> getImUsers_updateGroup");
		
		
		
		UserGroupAction<ImUser> updatesUsers = imuserDao.getIMUsers_update_group(RoleType.getAllType());
		
		System.out.println("-----<  counts >-----");
		System.out.println("total count : " + updatesUsers.getUsers().size());
				
		for(ImUser user : updatesUsers.getUsers()) {
			Map<String, ActionType> actionMap = updatesUsers.getActionMap(user);
			System.out.print( user.getUSR_ID() + "->");
			for(String groupName: actionMap.keySet()) {
				System.out.print( "(" + groupName+":"+ actionMap.get(groupName) +") ");
			}
			System.out.println();
		}
		
		System.out.println("<<<< ");
			
	}
	
	//@Test
	void getImUsers_updateGroupAuto() {		
		
		System.out.println(" >>>>>>>>>>>>>>> getImUsers_updateGroupAuto");
		
		
		UserGroupAction<ImUser> updatesUsers = imuserDao.getIMUsers_update_group(  RoleType.getNoRole());
		
		System.out.println("-----<  counts >-----");
		System.out.println("total count : " + updatesUsers.getUsers().size());
				
		for(ImUser user : updatesUsers.getUsers()) {
			Map<String, ActionType> actionMap = updatesUsers.getActionMap(user);
			System.out.print( user.getUSR_ID() + "->");
			for(String groupName: actionMap.keySet()) {
				System.out.print( "(" + groupName+":"+ actionMap.get(groupName) +") ");
			}
			System.out.println();
		}
		
		System.out.println("<<<< ");
			
	}
	
	//@Test
	void getImUsers() {
		
		
		System.out.println(" >>>>>>>>>>>>>>> getIMUsers");
		
		List<ImUser> users = imuserDao.getImUsers();
		
		for(ImUser user : users) {
			System.out.println( user.getUSR_ID() );
		}
		
		System.out.println("<<<< ");
			
	}
	
	//@Test
	void getImUsers_add() {
		
		
		System.out.println(" >>>>>>>>>>>>>>> getIMUsers_add");
		
		List<ImUser> users = imuserDao.getIMUsers_add();
		
		System.out.println(" >>>>>>>>>>>>>>> getIMUsers_add : " + users.size());
		
		for(ImUser user : users) {
			System.out.println( user.getUSR_ID() );
		}
		
		System.out.println("<<<< ");
		
	}
	
	@Test
	void getIMUsers_update()  {
		
		
		System.out.println(" >>>>>>>>>>>>>>> getIMUsers_update");
		
		List<ImUser> users = imuserDao.getIMUsers_update();
		
		System.out.println(" >>>>>>>>>>>>>>> getIMUsers_update : " + users.size());
		
		for(ImUser user : users) {
			System.out.println( user.getUSR_ID() );
		}
		
		System.out.println("<<<< ");
		
	}
	
	@Test
	void getAdminUsers() {
		
		System.out.println(" >>>>>>>>>>>>>>> getAdminUsers");
		
		JiraRestApi rest = new JiraRestApi();
		Map<String, CwdUser> adminUsers;
		try {
			adminUsers = rest.getJiraUsers("jira-administrators");
			
			for(String key : adminUsers.keySet()) {
				System.out.println( key );
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		

		
		System.out.println("<<<< ");
		
		
	}
	
}
