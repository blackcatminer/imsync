package com.hmm21.imsync;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import com.hmm21.action.UserGroupAction;
import com.hmm21.model.CwdUser;
import com.hmm21.model.ImUser;
import com.hmm21.restApi.JiraRestApi;
import com.hmm21.types.ActionType;

@SpringBootTest
class UpdateJiraUserGroupTests {

	@Autowired
	private IMSync imSync;
	
	@Test
	void updateUserGroup()  {
		
		System.out.println(" >>>>>>>>>>>>>>> updateUserGroup");
		
		imSync.updateJiraGroup( );
			
		
	}

}
