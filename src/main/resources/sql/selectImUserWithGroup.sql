
SELECT distinct `com_usr`.`USR_ID`,
    `com_usr`.`USR_PWD`,
    `com_usr`.`USR_NM`,
    `com_usr`.`USR_ENG_NM`,
    `com_usr`.`USR_DIV_CD`,
    `com_usr`.`USR_AUTH_TP_CD`,
    `com_usr`.`USR_PHN_NO`,
    `com_usr`.`USR_MPHN_NO`,
    `com_usr`.`USR_EML`,
    `com_usr`.`USR_TEAM_CD`,
    `com_usr`.`POSTN_ENG_NM`,
    `com_usr`.`FAX_NO`,
    `com_usr`.`USE_FLG`,
    `com_usr`.`NTN_CD`,
    `com_usr`.`LOC_CD`,
    `com_usr`.`JOB_ENG_NM`,
    `com_usr`.`OFCE_CD`,
    `com_usr`.`RHQ_OFCE_CD`,
    `com_usr`.`DEPT_CD`,
    `com_usr`.`PRNT_DEPT_CD`,
    `com_usr`.`TEAM_CD`,
    `com_usr`.`TEAM_PRNT_CD`,
    `com_usr`.`PRFCTR_CD`,
    `com_usr`.`LST_LGIN_OFCE_CD`,
    `com_usr`.`RSET_PWD_FLG`,
    `com_usr`.`OLD_SYS_ID`,
    `com_usr`.`IF_ID`,
    `com_usr`.`CRE_USR_ID`,
    `com_usr`.`CRE_DT`,
    `com_usr`.`UPD_USR_ID`,
    `com_usr`.`UPD_DT`,
	trim(case when `com_usr`.`USR_NM`= `com_usr`.`USR_ENG_NM` then `com_usr`.`USR_NM` else trim(BOTH '/' FROM  concat(`com_usr`.`USR_NM`,'/',`com_usr`.`USR_ENG_NM`)) end) as DISPLAY_NAME,
    group_concat(DISTINCT lower(`com_usr_has_role`.`role`) SEPARATOR  ';'  ) as `role`,
    group_concat(DISTINCT lower(`cwd_membership`.`lower_parent_name`) SEPARATOR  ';') as `jira_group`
    
FROM `imdb`.`com_usr`
INNER JOIN `jira`.`cwd_user` ON `cwd_user`.user_name = `com_usr`.`USR_ID` and cwd_user.directory_id=(select CS.id from jira.cwd_directory CS where CS.active=1 order by CS.directory_position asc limit 1)
LEFT JOIN `imdb`.`com_usr_has_role` ON `com_usr_has_role`.id = `com_usr`.`USR_ID`
LEFT JOIN `jira`.`cwd_membership` ON `cwd_membership`.lower_child_name = lower(`com_usr`.`USR_ID`) and cwd_user.directory_id=(select CS.id from jira.cwd_directory CS where CS.active=1 order by CS.directory_position asc limit 1)
group by `com_usr`.`USR_ID`
order by `com_usr`.`USR_ID`


