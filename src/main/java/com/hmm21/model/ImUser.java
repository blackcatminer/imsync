package com.hmm21.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import com.hmm21.types.HmmConsts;
import com.hmm21.types.IHasKey;

public class ImUser implements IHasKey<String> {
	private String USR_ID ;
	private String USR_PWD ;
	private String USR_NM ;
	private String USR_ENG_NM ;
	private String USR_DIV_CD ;
	private String USR_AUTH_TP_CD ;
	private String USR_PHN_NO ;
	private String USR_MPHN_NO ;
	private String USR_EML ;
	private String USR_TEAM_CD ;
	private String POSTN_ENG_NM ;
	private String FAX_NO ;
	private String USE_FLG ;
	private String NTN_CD ;
	private String LOC_CD ;
	private String JOB_ENG_NM ;
	private String OFCE_CD ;
	private String RHQ_OFCE_CD ;
	private String DEPT_CD ;
	private String PRNT_DEPT_CD ;
	private String TEAM_CD ;
	private String TEAM_PRNT_CD ;
	private String PRFCTR_CD ;
	private String LST_LGIN_OFCE_CD ;
	private String RSET_PWD_FLG ;
	private String OLD_SYS_ID ;
	private String IF_ID ;
	private String CRE_USR_ID ;
	private Date CRE_DT; 
	private String UPD_USR_ID ;
	private Date UPD_DT;	
	private String DISPLAY_NAME;
	private String ROLE;
	private String JIRA_GROUP;
	
	public String getUSR_ID() {
		return USR_ID;
	}
	public void setUSR_ID(String uSR_ID) {
		USR_ID = uSR_ID;
	}
	public String getUSR_PWD() {
		return USR_PWD;
	}
	public void setUSR_PWD(String uSR_PWD) {
		USR_PWD = uSR_PWD;
	}
	public String getUSR_NM() {
		return USR_NM;
	}
	public void setUSR_NM(String uSR_NM) {
		USR_NM = uSR_NM;
	}
	public String getUSR_ENG_NM() {
		return USR_ENG_NM;
	}
	public void setUSR_ENG_NM(String uSR_ENG_NM) {
		USR_ENG_NM = uSR_ENG_NM;
	}
	public String getUSR_DIV_CD() {
		return USR_DIV_CD;
	}
	public void setUSR_DIV_CD(String uSR_DIV_CD) {
		USR_DIV_CD = uSR_DIV_CD;
	}
	public String getUSR_AUTH_TP_CD() {
		return USR_AUTH_TP_CD;
	}
	public void setUSR_AUTH_TP_CD(String uSR_AUTH_TP_CD) {
		USR_AUTH_TP_CD = uSR_AUTH_TP_CD;
	}
	public String getUSR_PHN_NO() {
		return USR_PHN_NO;
	}
	public void setUSR_PHN_NO(String uSR_PHN_NO) {
		USR_PHN_NO = uSR_PHN_NO;
	}
	public String getUSR_MPHN_NO() {
		return USR_MPHN_NO;
	}
	public void setUSR_MPHN_NO(String uSR_MPHN_NO) {
		USR_MPHN_NO = uSR_MPHN_NO;
	}
	public String getUSR_EML() {
		return USR_EML;
	}
	public void setUSR_EML(String uSR_EML) {
		USR_EML = uSR_EML;
	}
	public String getUSR_TEAM_CD() {
		return USR_TEAM_CD;
	}
	public void setUSR_TEAM_CD(String uSR_TEAM_CD) {
		USR_TEAM_CD = uSR_TEAM_CD;
	}
	public String getPOSTN_ENG_NM() {
		return POSTN_ENG_NM;
	}
	public void setPOSTN_ENG_NM(String pOSTN_ENG_NM) {
		POSTN_ENG_NM = pOSTN_ENG_NM;
	}
	public String getFAX_NO() {
		return FAX_NO;
	}
	public void setFAX_NO(String fAX_NO) {
		FAX_NO = fAX_NO;
	}
	public String getUSE_FLG() {
		return USE_FLG;
	}
	public void setUSE_FLG(String uSE_FLG) {
		USE_FLG = uSE_FLG;
	}
	public String getNTN_CD() {
		return NTN_CD;
	}
	public void setNTN_CD(String nTN_CD) {
		NTN_CD = nTN_CD;
	}
	public String getLOC_CD() {
		return LOC_CD;
	}
	public void setLOC_CD(String lOC_CD) {
		LOC_CD = lOC_CD;
	}
	public String getJOB_ENG_NM() {
		return JOB_ENG_NM;
	}
	public void setJOB_ENG_NM(String jOB_ENG_NM) {
		JOB_ENG_NM = jOB_ENG_NM;
	}
	public String getOFCE_CD() {
		return OFCE_CD;
	}
	public void setOFCE_CD(String oFCE_CD) {
		OFCE_CD = oFCE_CD;
	}
	public String getRHQ_OFCE_CD() {
		return RHQ_OFCE_CD;
	}
	public void setRHQ_OFCE_CD(String rHQ_OFCE_CD) {
		RHQ_OFCE_CD = rHQ_OFCE_CD;
	}
	public String getDEPT_CD() {
		return DEPT_CD;
	}
	public void setDEPT_CD(String dEPT_CD) {
		DEPT_CD = dEPT_CD;
	}
	public String getPRNT_DEPT_CD() {
		return PRNT_DEPT_CD;
	}
	public void setPRNT_DEPT_CD(String pRNT_DEPT_CD) {
		PRNT_DEPT_CD = pRNT_DEPT_CD;
	}
	public String getTEAM_CD() {
		return TEAM_CD;
	}
	public void setTEAM_CD(String tEAM_CD) {
		TEAM_CD = tEAM_CD;
	}
	public String getTEAM_PRNT_CD() {
		return TEAM_PRNT_CD;
	}
	public void setTEAM_PRNT_CD(String tEAM_PRNT_CD) {
		TEAM_PRNT_CD = tEAM_PRNT_CD;
	}
	public String getPRFCTR_CD() {
		return PRFCTR_CD;
	}
	public void setPRFCTR_CD(String pRFCTR_CD) {
		PRFCTR_CD = pRFCTR_CD;
	}
	public String getLST_LGIN_OFCE_CD() {
		return LST_LGIN_OFCE_CD;
	}
	public void setLST_LGIN_OFCE_CD(String lST_LGIN_OFCE_CD) {
		LST_LGIN_OFCE_CD = lST_LGIN_OFCE_CD;
	}
	public String getRSET_PWD_FLG() {
		return RSET_PWD_FLG;
	}
	public void setRSET_PWD_FLG(String rSET_PWD_FLG) {
		RSET_PWD_FLG = rSET_PWD_FLG;
	}
	public String getOLD_SYS_ID() {
		return OLD_SYS_ID;
	}
	public void setOLD_SYS_ID(String oLD_SYS_ID) {
		OLD_SYS_ID = oLD_SYS_ID;
	}
	public String getIF_ID() {
		return IF_ID;
	}
	public void setIF_ID(String iF_ID) {
		IF_ID = iF_ID;
	}
	public String getCRE_USR_ID() {
		return CRE_USR_ID;
	}
	public void setCRE_USR_ID(String cRE_USR_ID) {
		CRE_USR_ID = cRE_USR_ID;
	}
	public Date getCRE_DT() {
		return CRE_DT;
	}
	public void setCRE_DT(Date cRE_DT) {
		CRE_DT = cRE_DT;
	}
	public String getUPD_USR_ID() {
		return UPD_USR_ID;
	}
	public void setUPD_USR_ID(String uPD_USR_ID) {
		UPD_USR_ID = uPD_USR_ID;
	}
	public Date getUPD_DT() {
		return UPD_DT;
	}
	public void setUPD_DT(Date uPD_DT) {
		UPD_DT = uPD_DT;
	}
	public String getROLE() {
		return ROLE;
	}
	
	public List<String> getListROLE() {
		
		if( this.ROLE == null || this.ROLE.isEmpty() ) {
			return new ArrayList<String>();
		}
		
		 return (List<String>) Arrays.asList(ROLE.split(HmmConsts.UserRoleSeperator));
	}
	public void setROLE(String rOLE) {
		ROLE = rOLE;
	}
	public String getDISPLAY_NAME() {
		return DISPLAY_NAME;
	}
	public void setDISPLAY_NAME(String dISPLAY_NAME) {
		DISPLAY_NAME = dISPLAY_NAME;
	}
	public String getJIRA_GROUP() {
		return JIRA_GROUP;
	}
	public List<String> getListJIRA_GROUP() {
		
		if( this.JIRA_GROUP == null || this.JIRA_GROUP.isEmpty() ) {
			return new ArrayList<String>();
		}
		
		 return (List<String>) Arrays.asList(JIRA_GROUP.split(HmmConsts.UserGorupSeperator));
	}
	public void setJIRA_GROUP(String jIRA_GROUP) {
		JIRA_GROUP = jIRA_GROUP;
	}
	
	
	@Override
	public String getKey() {		
		return this.USR_ID;
	}
}
