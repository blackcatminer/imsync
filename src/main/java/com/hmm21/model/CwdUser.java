package com.hmm21.model;

import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;


public class CwdUser {
	@JsonProperty("name")
	private String name;
	
	@JsonProperty("displayName")
	private String displayName ;
	
	@JsonProperty("emailAddress")
	private String emailAddress ;
	
	@JsonProperty("password")
	private String password;
	
	@JsonProperty("active")
	private boolean active; 
	private Date createdDate;
	private Date updatedDate;
	
	// 데이터 검증
	public boolean isValid() {
		if( getInValidCause() == null ) {
			return true;
		}
		
		return false;
	}
	public String getInValidCause() {
		if( name == null || name.length() <= 0 ) {
			return "사용자 이름이 정상적이지 않음 ";
		}
		if( displayName == null || displayName.length() <= 0 ) {
			return "displayName이 정상적이지 않음 ";
		}
		if( emailAddress == null || emailAddress.length() <= 0 ) {
			return "emailAddress이 정상적이지 않음 ";
		}
		if( password == null || password.length() <= 0 ) {
			return "password이 정상적이지 않음";
		}
		
		return null;
	}
	
			
	public String getName() {
		return name;
	}
	public CwdUser setName(String name) {
		this.name = name;
		return this;
	}
	public String getDisplayName() {
		return displayName;
	}
	public CwdUser setDisplayName(String displayName) {
		this.displayName = displayName;
		return this;
	}
	public String getEmailAddress() {
		return emailAddress;
	}
	public CwdUser setEmailAddress(String emailAddress) {
		this.emailAddress = emailAddress;
		return this;
	}
	public String getPassword() {
		return password;
	}
	public CwdUser setPassword(String password) {
		this.password = password;
		return this;
	}

	public Date getCreatedDate() {
		return createdDate;
	}
	public CwdUser setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
		return this;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public CwdUser setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
		return this;
	}
	
	public static CwdUser Build() {
		return new CwdUser();
	}
	public boolean isActive() {
		return active;
	}
	public CwdUser setActive(boolean active) {
		this.active = active;
		return this;
	}
	
	
}
