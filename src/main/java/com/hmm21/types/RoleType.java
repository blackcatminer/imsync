package com.hmm21.types;

import java.util.Arrays;
import java.util.List;

public enum RoleType {
	
	CUSTOM,
	HEAD,
	BA,
	IT,
	LEADER,
	KEYMAN,
	DEV
	;
	
	public static List<RoleType> getAllType(){
		return Arrays.asList(RoleType.BA,RoleType.LEADER,RoleType.IT,RoleType.KEYMAN,RoleType.DEV,RoleType.HEAD, RoleType.CUSTOM);
	}
	
	public static List<RoleType> getNoRole(){
		return Arrays.asList(RoleType.HEAD, RoleType.CUSTOM);
	}
	
}


