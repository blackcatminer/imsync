package com.hmm21.types;

public interface IHasKey<T> {
	public T getKey();
}

