package com.hmm21.types;

public enum ActionType {
	NONE, ADD, REMOVE, UPDATE
}
