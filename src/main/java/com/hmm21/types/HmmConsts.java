package com.hmm21.types;

import java.util.List;

/*
 * 프로젝트의 상수
 */

public final class HmmConsts {
	public final static String UserRoleSeperator = ";";
	public final static String UserGorupSeperator = ";";
	
	
	/*
	 * Role 이름
	 */
	public static String GetRoleName(RoleType type) {
		
		switch(type) {
			case CUSTOM: return "";
			case HEAD: return "";
			case BA: return "BA";
			case IT: return "IT";
			case DEV: return "DEV";
			case LEADER: return "LEADER";
			case KEYMAN: return "KEYMAN";
			default: return "";
		}
		
	}
	
	public static RoleType GetRoleType(String role, String nation_cd) {
		
		if(role == null ) {
			role = "";
		}
		
		switch(role.toUpperCase()) {
			case "BA" : return RoleType.BA;
			case "IT": return RoleType.IT;
			case "LEADER": return RoleType.LEADER;
			case "DEV": return RoleType.DEV;
			case "KEY": return RoleType.KEYMAN;
			default : 
			{
				switch(nation_cd) {
				case "KR": return RoleType.HEAD;
				default : return RoleType.CUSTOM;
				}
			}
			
		}
	}
	
	
	public static boolean HasRole(List<String> listRole, String nation_cd, RoleType type) {
		
		
		switch(type) {
		case HEAD:{
			if( nation_cd == null || nation_cd.isEmpty() || nation_cd.toUpperCase() == "KR" ) {
				if( listRole == null || listRole.size() == 0) {
					return true;
				} 
			}
		} break;
		case CUSTOM:{
			if( nation_cd != null && nation_cd.isEmpty() == false && nation_cd.toUpperCase() != "KR" ) {
				if( listRole == null || listRole.size() == 0) {
					return true;
				} 
			}
		} break;
		default:
			break;
		
		}
		
		if( listRole == null || listRole.size() == 0) {
			return false;
		} else {
			return listRole.contains(GetRoleName(type).toLowerCase());
		}
		
	}
	
	/*
	 * Role 이름
	 */
	public static String GetGroupName(RoleType type) {
		
		// 없음-> custom group
		// head -> Head Group
		// BA ->  BA Group
		// IT -> IT Group
		// LEADER -> LEDER Group
		// DEV -> Dev Group
		
		switch(type) {
			case CUSTOM: return "custom group";
			case BA: return "BA Group";
			case IT: return "IT Group";
			case DEV: return "Dev Group";
			case LEADER: return "Leader Group";
			case HEAD: return "Head Group";
			case KEYMAN: return "Keyman Group";
			default: return "";
		}
		
	}
	public static boolean HasGroup(List<String> listGroup, RoleType type ) {
		return listGroup.contains(GetGroupName(type).toLowerCase());
	}
	
}

