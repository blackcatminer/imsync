package com.hmm21.action;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;


import com.hmm21.types.ActionType;
import com.hmm21.types.IHasKey;

public class UserGroupAction<T extends IHasKey<String> > {
	
	
	
	//
	private T user;
	
	 
	// 사용자key - 사용자 객체
	private Map<String, T> userMap = new HashMap<String, T>();
	
	// 사용자key - 그룸 - 액션
	private Map<String, Map<String, ActionType> > actionMap = new HashMap<String, Map<String, ActionType> >();
	
	//
	public Collection<T> getUsers(){
		return this.userMap.values();
	}
	
	public UserGroupAction<T> setAction(T user, String groupName, ActionType type) {
		
		if( groupName == null || groupName.isEmpty() ) {
			// TODO : log 
			System.err.println("UserGroupAction groupName is empty");
			return this;
		}
		
		if( user == null || user.getKey() == null || user.getKey().isEmpty() ) {
			// TODO : log 
			System.err.println("UserGroupAction user is wrong");
			return this;
		}
		
		//
		this.userMap.put(user.getKey(), user);
		//
		Map<String, ActionType> action = this.actionMap.get(user.getKey());
		if( action == null) {
			action = new HashMap<String, ActionType>();
			this.actionMap.put(user.getKey(), action);
		}		
		action.put(groupName, type);	
		
		return this;
	}
	
	public Map<String, ActionType> getActionMap(T user){
		Map<String, ActionType> action = this.actionMap.get(user.getKey());
		if( action == null) {
			return new HashMap<String, ActionType>();
		}
		
		return action;
	}
	
	public ActionType getAction(T user, String groupName) {
		
		if( groupName == null || groupName.isEmpty() ) {
			// TODO : log 
			System.err.println("UserGroupAction groupName is empty");
			return ActionType.NONE;
		}
		
		Map<String, ActionType> action = this.actionMap.get(user.getKey());
		if( action == null) {
			return ActionType.NONE;
		}
		
		ActionType type = action.get(groupName);
		if( type == null ) {
			return ActionType.NONE;
		}
		
		return type;		
	}
	
	public T getUser(String userName) {
		return this.userMap.get(userName);	
	}
	
}
