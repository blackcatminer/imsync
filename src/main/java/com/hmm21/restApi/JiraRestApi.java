package com.hmm21.restApi;


import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLDecoder;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.JsonParser;
import org.springframework.boot.json.JsonParserFactory;
import org.springframework.stereotype.Repository;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hmm21.model.CwdUser;
import com.hmm21.util.RestApiMgr;

@Repository
public class JiraRestApi {
	
	@Value("${url.jirabase}")
	private String baseJiraUrl = "http://itsm.hmm21.com/jira";
	
	private final String baseEncoding = "UTF-8";
	
	
//	@Autowired
//    private RestTemplateBuilder restTemplateBuilder;
	
	public boolean updateUser(CwdUser user) throws Exception {
		ObjectMapper mapper = new ObjectMapper();
		
		Map<String, Object> map = new HashMap<String, Object>();
		
		// TODO update에서 name, password는 제외
		//map.put("name", user.getName());
		// map.put("password", user.getPassword());		
		map.put("emailAddress", user.getEmailAddress());
		map.put("displayName", user.getDisplayName());
		
	
		String sendData = mapper.writeValueAsString(map);
		System.out.println(sendData);
		
		URL url = new URL(baseJiraUrl + "/rest/api/2/user?username="+URLEncoder.encode(user.getName(),baseEncoding ));
		String response = RestApiMgr.Inst().sendREST("PUT", url.toString(), sendData);
		System.out.println(response);
		return true;
	}
	
	public boolean insertUser(CwdUser user) throws Exception  {
			

			ObjectMapper mapper = new ObjectMapper();
		
			Map<String, Object> map = new HashMap<String, Object>();
			map.put("name", user.getName());
			//map.put("password", user.getPassword());
			// TODO 임시 패스워를 id로 
			map.put("password", user.getName());
			map.put("emailAddress", user.getEmailAddress());
			map.put("displayName", user.getDisplayName());
			map.put("applicationKeys", new String[] {});
		
			String sendData = mapper.writeValueAsString(map);
			System.out.println(sendData);
			
			URL url = new URL(baseJiraUrl + "/rest/api/2/user");
			String response = RestApiMgr.Inst().sendRESTPost(url.toString(), sendData);
			System.out.println(response);

				
		return true;
	}
	
	public String addUserToGroup(CwdUser user, String groupName) throws Exception {
		
			ObjectMapper mapper = new ObjectMapper();

			Map<String, String> map = new HashMap<String, String>();
			map.put("name", user.getName());
		
			String sendData = mapper.writeValueAsString(map);
			System.out.println(sendData);
			
			URL url = new URL(baseJiraUrl+"/rest/api/2/group/user?groupname="+URLEncoder.encode(groupName, "UTF-8"));
			String response = RestApiMgr.Inst().sendRESTPost(url.toString(), sendData);

			return response;
	}
	public String removeUserToGroup(CwdUser user, String groupName) throws Exception {

			ObjectMapper mapper = new ObjectMapper();
				
			URL url = new URL(baseJiraUrl+"/rest/api/2/group/user?groupname="+URLEncoder.encode(groupName, "UTF-8")+"&username="+URLEncoder.encode(user.getName(), "UTF-8"));
			return RestApiMgr.Inst().sendREST(url.toString(), "", "DELETE");
			
	}
	
	public String deleteUser(CwdUser user) throws Exception {
		   	
		URL url = new URL(baseJiraUrl + "/rest/api/2/user?username="+user.getName());
		return RestApiMgr.Inst().sendREST(url.toString(), "", "DELETE");
	}
	
	public Map<String, CwdUser> getJiraUsers(String groupName) throws Exception{
		
		Map<String, CwdUser> listUser = new HashMap<String,CwdUser>();
		
			
		ObjectMapper mapper = new ObjectMapper();
					
		URL url = new URL(baseJiraUrl + "/rest/api/2/group/member?groupname="+URLEncoder.encode(groupName, "UTF-8"));
		System.out.println(url);
		String response = RestApiMgr.Inst().sendRESTGet(url.toString());
		
		System.out.println(response);
		
		JsonParser springParser = JsonParserFactory.getJsonParser();
		Map<String, Object> map = springParser.parseMap(response);
		
		List<?> listUsers = (List<?>)map.get("values");
		
		for(Object item : listUsers) {
			Map<String, Object> user = (Map<String, Object>)item;
			if( user == null ) {
				continue;
			}
			String userName = (String)user.get("name");
			if( userName == null || userName.isEmpty() ) {
				continue;
			}
			listUser.put(userName, new CwdUser().setName(userName));
			
		}

						
		return listUser;
		
	}
	
	
	
	/*
	 * jira에서 사용하는 rest api 형식
	 *  - paging 기능을 활용하여 한번에 조회 되지 않은 데이터를 연속으로 조회
	 * 
	 * maxResults : 한번에 조회되는 최대 개수
	 * startAt : 시작 offset
	 * total : 조회되는 총 개수(startAt에 영향 없음)
	 */
	protected String sendRESTGetUtilLast(String sendUrl, String itemName) throws Exception {

		Integer total = 0;
		Integer startAt = 0;
		Integer maxResults = 50;
		
		while(true) {
			String response = RestApiMgr.Inst().sendRESTGet(sendUrl + "&startAt="+startAt+"&maxResults="+maxResults);
			
			JsonParser springParser = JsonParserFactory.getJsonParser();
			Map<String, Object> map = springParser.parseMap(response);
			
			total = (Integer) map.get("total");
			startAt = (Integer) map.get("startAt");
			maxResults = (Integer) map.get("maxResults");
		
			if(startAt + maxResults >= total) {
				break;
			}
			
			startAt += maxResults;
			
			//map.get(itemName)
			
			
			
		}
		
		return "";
	}
	
	
}

