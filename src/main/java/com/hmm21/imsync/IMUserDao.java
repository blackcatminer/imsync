package com.hmm21.imsync;

import java.io.File;
import java.nio.file.Files;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.hmm21.action.UserGroupAction;
import com.hmm21.model.*;
import com.hmm21.types.ActionType;
import com.hmm21.types.HmmConsts;
import com.hmm21.types.RoleType;
import com.hmm21.util.FileUtil;

@Repository
public class IMUserDao {
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	
	/* 
	 * IM 사용자 
	 */
	public List<ImUser> getImUsers() {
		
		try {
		Resource resource = new ClassPathResource("sql/selectImUser.sql");
		String query = FileUtil.readFile(resource.getInputStream());

		System.out.println(query);
		
		return jdbcTemplate.query(query, new BeanPropertyRowMapper<ImUser>(ImUser.class));
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ArrayList<ImUser>();
		}
	}
	
	/*
	 * 추가된 IM 사용자
	 */
	public List<ImUser> getIMUsers_add() {
		try {
			Resource resource = new ClassPathResource("sql/selectImUser_add.sql");
			String query = FileUtil.readFile(resource.getInputStream());
	
			System.out.println(query);
			
			return jdbcTemplate.query(query, new BeanPropertyRowMapper<ImUser>(ImUser.class));
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ArrayList<ImUser>();
		}
	}
	
	/*
	 * 정보가 변경된 IM 사용자
	 */
	public List<ImUser> getIMUsers_update(){
		try {
			Resource resource = new ClassPathResource("sql/selectImUser_update.sql");
			String query = FileUtil.readFile(resource.getInputStream());
	
			System.out.println(query);
			
			return jdbcTemplate.query(query, new BeanPropertyRowMapper<ImUser>(ImUser.class));
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ArrayList<ImUser>();
		}
	}
	
	/*
	 * group 변경된 사용자
	 * 
	 *   
	 */
	public UserGroupAction<ImUser> getIMUsers_update_group(List<RoleType> checkRoleList){
		
		UserGroupAction<ImUser> updateUsers = new UserGroupAction<ImUser>();
		
		if(checkRoleList == null) {
			return updateUsers;
		}
		
		try {
			Resource resource = new ClassPathResource("sql/selectImUserWithGroup.sql");
			String query = FileUtil.readFile(resource.getInputStream());
	
			System.out.println(query);
			
			List<ImUser> listImUser = jdbcTemplate.query(query, new BeanPropertyRowMapper<ImUser>(ImUser.class)); 

			for(ImUser imUser : listImUser ) {
				
				List<String> roles = imUser.getListROLE();
				List<String> jiraGroups = imUser.getListJIRA_GROUP();

;
				
				for(RoleType type: checkRoleList ) {
					boolean hasRole = HmmConsts.HasRole(roles, imUser.getNTN_CD(), type);
					boolean hasGroup = HmmConsts.HasGroup(jiraGroups, type);
					if(hasRole != hasGroup) {					
						updateUsers.setAction(imUser, HmmConsts.GetGroupName(type), hasRole? ActionType.ADD:ActionType.REMOVE);
					}
				}
				
																	
			}
			
		}catch(Exception ex) {
			ex.printStackTrace();			
		}
		
		return updateUsers;
	}
	
	/*
	 * CWUser
	 */
	public List<CwdUser> getCwdUsers(){
		try {
			Resource resource = new ClassPathResource("sql/selectCwdUser.sql");
			String query = FileUtil.readFile(resource.getInputStream());
	
			System.out.println(query);
			
			return jdbcTemplate.query(query, new BeanPropertyRowMapper<CwdUser>(CwdUser.class));
		}catch(Exception ex) {
			ex.printStackTrace();
			return new ArrayList<CwdUser>();
		}
	}

}
