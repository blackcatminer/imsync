package com.hmm21.imsync;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ImSyncApplication {

	@Autowired
	private static IMSync imSync;
	
	public static void main(String[] args) {
		
		
		SpringApplication app = new SpringApplication(ImSyncApplication.class);
		if(args == null || args.length < 1) {
			
			app.run(args);
			return;
		}
		
		
		switch(args[1]) {
		case "-insert": imSync.insertJiraUsers(); break;
		case "-update": imSync.updateJiraUsers(); break; 
		case "-gupdate": imSync.updateJiraGroupNoRole(); break;
		case "-deleteAll": imSync.updateJiraGroup(); break;
		default: break;
		}
		
		
		
		//SpringApplication app = new SpringApplication(ImSyncApplication.class);
	    //app.run(args);
	}
	
	
}
