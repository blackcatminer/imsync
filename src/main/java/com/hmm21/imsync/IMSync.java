package com.hmm21.imsync;


import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hmm21.action.UserGroupAction;
import com.hmm21.imsync.IMUserDao;
import com.hmm21.model.CwdUser;
import com.hmm21.model.ImUser;
import com.hmm21.restApi.JiraRestApi;
import com.hmm21.types.ActionType;
import com.hmm21.types.RoleType;


@Repository
public class IMSync {
	
	@Autowired
	private IMUserDao imUser;

	private JiraRestApi rest = new JiraRestApi();
	
	/**
	 * im user와 비교해서 jira에 없는 유저를 추가
	 * 
	 */
	public void insertJiraUsers() {
		// 추가된 사용자
				HashMap<String, ImUser> user_added = new HashMap<String, ImUser>();
				// 처리하지 못한 사용자
				HashMap<String, ImUser> user_error = new HashMap<String, ImUser>();
				HashMap<String, String> user_error_cause = new HashMap<String, String>();
				
				// 사용자 리스트 가져옴
				List<ImUser> users = imUser.getIMUsers_add();
				System.out.println(">>>>>>> Total IM User count : " + users == null ? 0 : users.size() ); 
				for(ImUser user : users) {

					System.out.println("user : " +  user.getUSR_ID() + "(" + "Y".equals(user.getUSE_FLG()) + ")" );
					
					CwdUser cwdUser = new CwdUser()
							.setName(user.getUSR_ID())
							.setPassword(user.getUSR_PWD())
							.setDisplayName(user.getDISPLAY_NAME())
							.setEmailAddress(user.getUSR_EML())
							.setActive("Y".equals(user.getUSE_FLG()) )
							;
					// 
					if( cwdUser.isValid() == false ) {
						user_error.put(user.getUSR_ID(), user);
						user_error_cause.put(user.getUSR_ID(), cwdUser.getInValidCause() );
						continue;
					}
					
					if( cwdUser.isActive() == false ) {
						user_error.put(user.getUSR_ID(), user);
						user_error_cause.put(user.getUSR_ID(), "비활성 유저" );
						continue;
					}
					try {
						
						rest.insertUser(cwdUser);				
						user_added.put(user.getUSR_ID(), user);
						
					}catch(Exception ex) {
						user_error.put(user.getUSR_ID(), user);
						user_error_cause.put(user.getUSR_ID(), ex.getMessage() );
					}
					
				}
				
				System.out.println("");
				System.out.println(">>>>>>>>>  Insert Result <<<<<<<<<<< ");
				System.out.println("add total count : " + user_added.size());
				System.out.println("error total count : " + user_error.size());
				System.out.println("-----< added user >-----");
				for(ImUser user : user_added.values()) {
					System.out.println(user.getUSR_ID());
				}
				System.out.println("-----< error user >-----");
				for(ImUser user : user_error.values()) {
					System.out.println(user.getUSR_ID() + ":" + user_error_cause.get(user.getUSR_ID()) );
				}
	}
	
	public void updateJiraUsers() {
		// 정상 작업 완료된 사용자
		HashMap<String, ImUser> user_works = new HashMap<String, ImUser>();
		
		// 처리하지 못한 사용자
		HashMap<String, ImUser> user_error = new HashMap<String, ImUser>();
		HashMap<String, String> user_error_cause = new HashMap<String, String>();
		
		// 사용자 리스트 가져옴
		List<ImUser> updatesUsers = imUser.getIMUsers_update();
				
		for(ImUser user : updatesUsers) {
		
			CwdUser cwdUser = new CwdUser()
					.setName(user.getUSR_ID())
					.setPassword(user.getUSR_PWD())	
					.setDisplayName(user.getDISPLAY_NAME())
					.setEmailAddress(user.getUSR_EML())
					.setActive(user.getUSE_FLG() == "Y")
					;
			try {
								
				rest.updateUser(cwdUser); 
				user_works.put(user.getUSR_ID(), user);
						
			}catch(Exception ex) {
				user_error.put(user.getUSR_ID(), user);
				user_error_cause.put(user.getUSR_ID(), ex.getMessage() );
			}
			
			
		}
		
		System.out.println("");
		System.out.println(">>>>>>>>> Result <<<<<<<<<<< ");
		System.out.println("-----<  counts >-----");
		System.out.println("total count : " + (user_works.size()  + user_error.size()) );
		System.out.println("works count : " + user_works.size() );
		System.out.println("error total count : " + user_error.size());
		
		System.out.println("-----<  works user >-----");
		for(ImUser user : user_works.values()) {
			System.out.println(user.getUSR_ID() );
		}
		
		System.out.println("-----< error user >-----");
		for(ImUser user : user_error.values()) {
			System.out.println(user.getUSR_ID() + ":" + user_error_cause.get(user.getUSR_ID()) );
		}
					
	}
	
	public void updateJiraGroup() {		
		updateJiraGroup(RoleType.getAllType());
	}
	public void updateJiraGroupNoRole() {
		updateJiraGroup(RoleType.getNoRole());
	}
	public void updateJiraGroup(List<RoleType> listRoleType) {
		
		
		// 정상 작업 완료된 사용자
		HashMap<String, ImUser> user_works_add = new HashMap<String, ImUser>();
		HashMap<String, String> user_works_add_group = new HashMap<String, String>();
		HashMap<String, ImUser> user_works_remove = new HashMap<String, ImUser>();
		HashMap<String, String> user_works_remove_group = new HashMap<String, String>();
		
		// 처리하지 못한 사용자
		HashMap<String, ImUser> user_error = new HashMap<String, ImUser>();
		HashMap<String, String> user_error_cause = new HashMap<String, String>();
		
		// 사용자 리스트 가져옴
		UserGroupAction<ImUser> updatesUsers = imUser.getIMUsers_update_group(listRoleType);
		
		for(ImUser user : updatesUsers.getUsers()) {
			
			Map<String,  ActionType> actionMap = updatesUsers.getActionMap(user);
			
			System.out.print( user.getUSR_ID() + "->");
			for(String groupName: actionMap.keySet()) {
				
				ActionType actionType = actionMap.get(groupName);
				
				System.out.print( "(" + groupName+":"+ actionType +") ");
				
				CwdUser cwdUser = new CwdUser()
						.setName(user.getUSR_ID())
						.setPassword(user.getUSR_PWD())
						.setDisplayName(user.getDISPLAY_NAME())
						.setEmailAddress(user.getUSR_EML())
						.setActive(user.getUSE_FLG() == "Y")
						;
				try {
				
					switch(actionType) {
						case ADD: 
						{
							user_works_add_group.put(user.getUSR_ID(), groupName);
							rest.addUserToGroup(cwdUser, groupName); 
							user_works_add.put(user.getUSR_ID(), user);
							
							
						} break;
						case REMOVE: 
						{
							user_works_remove_group.put(user.getUSR_ID(), groupName);
							rest.removeUserToGroup(cwdUser, groupName); 
							user_works_remove.put(user.getUSR_ID(), user);
						} break;
					}		
				}catch(Exception ex) {
					user_error.put(user.getUSR_ID(), user);
					user_error_cause.put(user.getUSR_ID(), actionType + ":" +groupName+  ex.getMessage() );
				}
			}
			System.out.println();
			
			
		}
		
		System.out.println("");
		System.out.println(">>>>>>>>> Result <<<<<<<<<<< ");
		System.out.println("-----<  counts >-----");
		System.out.println("total count : " + (user_works_add.size() + user_works_remove.size() + user_error.size()) );
		System.out.println("works(add) count : " + user_works_add.size() );
		System.out.println("works(remove) count : " + user_works_remove.size() );
		System.out.println("error total count : " + user_error.size());
		
		System.out.println("-----<  works(add) user >-----");
		for(ImUser user : user_works_add.values()) {
			System.out.println(user.getUSR_ID() + ":" + user_works_add_group.get(user.getUSR_ID()) );
		}
		
		System.out.println("-----<  works(remove) user >-----");
		for(ImUser user : user_error.values()) {
			System.out.println(user.getUSR_ID() + ":" + user_works_add_group.get(user.getUSR_ID()) );
		}
		
		System.out.println("-----< error user >-----");
		
		for(ImUser user : user_error.values()) {
			
			System.out.println(user.getUSR_ID() + ":" + user_error_cause.get(user.getUSR_ID()) );
		}
	}
	
	

	/**
	 * 비활성이 아닌 user 삭제
	 * (비활성은 update로 처리한다)
	 * @throws UnsupportedEncodingException 
	 * @throws MalformedURLException 
	 */
	public void deleteJiraUsers() throws Exception {
		
		Map<String,CwdUser> adminUsers = rest.getJiraUsers("jira-administrators");
		
		// 처리한 사용자
		HashMap<String, ImUser> user_works = new HashMap<String, ImUser>();
		// 처리하지 못한 사용자
		HashMap<String, ImUser> user_error = new HashMap<String, ImUser>();
		HashMap<String, String> user_error_cause = new HashMap<String, String>();
		
	
		
		// 사용자 리스트 가져옴
		List<ImUser> users = imUser.getImUsers();
		System.out.println(" User count : " + users == null ? 0 : users.size() ); 
		for(ImUser user : users) {

			
			System.out.println("user : " +  user.getUSR_ID() );
			
			if( adminUsers.containsKey(user.getUSR_ID()) == true ) {
				user_error.put(user.getUSR_ID(), user);
				user_error_cause.put(user.getUSR_ID(), "Admin user" );
				continue;
			}
			
			CwdUser cwdUser = new CwdUser()
					.setName(user.getUSR_ID())
					.setPassword(user.getUSR_PWD())
					.setDisplayName(user.getDISPLAY_NAME())
					.setEmailAddress(user.getUSR_EML())
					.setActive("Y".equals(user.getUSE_FLG()) )
					;
			
			// 
			try {
				rest.deleteUser(cwdUser);
				user_works.put(user.getUSR_ID(), user);
			}catch(Exception ex) {
				user_error.put(user.getUSR_ID(), user);
				user_error_cause.put(user.getUSR_ID(), ex.getMessage() );
			}
		}
		
		System.out.println("");
		System.out.println(">>>>>>>>>  Result <<<<<<<<<<< ");
		System.out.println("-----< delete user >-----");
		System.out.println(" total count : " + user_works.size());
		System.out.println("-----< error >-----");
		System.out.println("error total count : " + user_error.size());
		System.out.println("-----< error user >-----");
		
		for(ImUser user : user_error.values()) {
			
			System.out.println(user.getUSR_ID() + ":" + user_error_cause.get(user.getUSR_ID()) );
		}
		
	}
	
	/**
	 * 비활성이 아닌 user 삭제
	 * (비활성은 update로 처리한다)
	 * @throws UnsupportedEncodingException 
	 * @throws MalformedURLException 
	 */
	public void deleteJiraUsers(String deleteuserInGroup) throws Exception {
		
		Map<String,CwdUser> adminUsers = rest.getJiraUsers("jira-administrators");
		
		// 처리한 사용자
		HashMap<String, CwdUser> user_works = new HashMap<String, CwdUser>();
		// 처리하지 못한 사용자
		HashMap<String, CwdUser> user_error = new HashMap<String, CwdUser>();
		HashMap<String, String> user_error_cause = new HashMap<String, String>();
		
	
		
		// 사용자 리스트 가져옴
		Map<String,CwdUser> deleteUsers =  rest.getJiraUsers(deleteuserInGroup);
		System.out.println(" User count : " + deleteUsers == null ? 0 : deleteUsers.size() ); 
		for(CwdUser cwUser : deleteUsers.values()) {

			
			System.out.println("user : " +  cwUser.getName() );
			
			if( adminUsers.containsKey(cwUser.getName()) == true ) {
				user_error.put(cwUser.getName(), cwUser);
				user_error_cause.put(cwUser.getName(), "Admin user" );
				continue;
			}
			
			
			
			// 
			try {
				rest.deleteUser(cwUser);
				user_works.put(cwUser.getName(), cwUser);
			}catch(Exception ex) {
				user_error.put(cwUser.getName(), cwUser);
				user_error_cause.put(cwUser.getName(), ex.getMessage() );
			}
		}
		
		System.out.println("");
		System.out.println(">>>>>>>>>  Result <<<<<<<<<<< ");
		System.out.println("-----< delete user >-----");
		System.out.println(" total count : " + user_works.size());
		System.out.println("-----< error >-----");
		System.out.println("error total count : " + user_error.size());
		System.out.println("-----< error user >-----");
		
		for(CwdUser user : user_error.values()) {
			
			System.out.println(user.getName() + ":" + user_error_cause.get(user.getName()) );
		}
		
	}
	
	
	/**
	 * 비활성이 아닌 user 삭제
	 * (비활성은 update로 처리한다)
	 * @throws UnsupportedEncodingException 
	 * @throws MalformedURLException 
	 */
	public void deleteJiraUsersAllExceptinAdmin() throws Exception {
		
		Map<String,CwdUser> adminUsers = rest.getJiraUsers("jira-administrators");
		
		// 처리한 사용자
		HashMap<String, CwdUser> user_works = new HashMap<String, CwdUser>();
		// 처리하지 못한 사용자
		HashMap<String, CwdUser> user_error = new HashMap<String, CwdUser>();
		HashMap<String, String> user_error_cause = new HashMap<String, String>();
		
	
		
		// 사용자 리스트 가져옴
		List<CwdUser> deleteUsers =  imUser.getCwdUsers();
		System.out.println(" User count : " + deleteUsers == null ? 0 : deleteUsers.size() ); 
		for(CwdUser cwUser : deleteUsers) {

			
			System.out.println("user : " +  cwUser.getName() );
			
			if( adminUsers.containsKey(cwUser.getName()) == true ) {
				user_error.put(cwUser.getName(), cwUser);
				user_error_cause.put(cwUser.getName(), "Admin user" );
				continue;
			}
			
			// 
			try {
				rest.deleteUser(cwUser);
				user_works.put(cwUser.getName(), cwUser);
			}catch(Exception ex) {
				user_error.put(cwUser.getName(), cwUser);
				user_error_cause.put(cwUser.getName(), ex.getMessage() );
			}
		}
		
		System.out.println("");
		System.out.println(">>>>>>>>>  Result <<<<<<<<<<< ");
		System.out.println("-----< delete user >-----");
		System.out.println(" total count : " + user_works.size());
		System.out.println("-----< error >-----");
		System.out.println("error total count : " + user_error.size());
		System.out.println("-----< error user >-----");
		
		for(CwdUser user : user_error.values()) {
			
			System.out.println(user.getName() + ":" + user_error_cause.get(user.getName()) );
		}
		
	}

}
