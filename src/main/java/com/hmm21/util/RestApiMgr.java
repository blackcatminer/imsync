package com.hmm21.util;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public class RestApiMgr {
	private RestApiMgr() {}
	private static RestApiMgr mgr = new RestApiMgr();
	public static RestApiMgr Inst() {
		return mgr; 
	}
	
	private final String baseAuthKey = "Basic c3RsaW06aG1tMjEjIw==";
	
	public String sendRESTPost(String sendUrl, String jsonValue) throws Exception {

		return sendREST(sendUrl, jsonValue, "POST");
	}
	
	public String sendRESTGet(String sendUrl) throws Exception {

		return sendREST(sendUrl, null, "GET");
	}
	
	
	public String sendREST(String sendUrl, String jsonValue, String method) throws Exception {

		String resMsg = "";
		String inputLine = null;
		StringBuffer outResult = new StringBuffer();

		
			URL url = new URL(sendUrl);
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod (method);
			conn.setRequestProperty("Content-Type", "application/json");
			conn.setRequestProperty("Accept-Charset", "UTF-8"); 
			conn.setRequestProperty("authorization", baseAuthKey); 
			conn.setConnectTimeout(10000);
			conn.setReadTimeout(10000);
		      
			if(jsonValue != null && jsonValue.length() > 0) {
				OutputStream os = conn.getOutputStream();
				os.write(jsonValue.getBytes("UTF-8"));
				os.flush();
			}
		    
			try {
			// 리턴된 결과 읽기
			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream(), "UTF-8"));
			while ((inputLine = in.readLine()) != null) {
				outResult.append(inputLine);
			}
		    
			} finally {
			
				try {
				if( conn != null ) {			
					conn.disconnect();
				}
				}catch(Exception ex){}
			}
			   	
		  
		  return outResult.toString();
		}
}
