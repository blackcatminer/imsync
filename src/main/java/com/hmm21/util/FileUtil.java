package com.hmm21.util;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileUtil {
    public static void main(String[] args) throws IOException {
        System.out.println(readFile("/sql/selectImUser.sql"));
    }

    public static String readFile(String classpath) throws IOException {
        return readFile(FileUtil.class.getResourceAsStream(classpath));
    }

    public static String readFile(InputStream is) throws IOException {
        ByteArrayOutputStream os = null;

        int size = 1024;
        byte[] buffer = new byte[size];
        int length = -1;
        String retValue = null;

        try {
            os = new ByteArrayOutputStream();

            while ((length = is.read(buffer)) != -1) {
                os.write(buffer, 0, length);
            }
        } catch (IOException ioe) {
            throw ioe;
        } finally {
            if (is != null)
                is.close();
            if (os != null) {
                retValue = os.toString();
                os.close();
            }
        }

        return retValue;
    }

}
